
package edabit_nivel_intermedio;

        import java.util.stream.IntStream;
public class Programa_1 {
	public static int[] arrayOfMultiples(int num, int length) {
		return IntStream.rangeClosed(1, length)
			.map(i -> num * i)
			.toArray();
	}
}
    
    

