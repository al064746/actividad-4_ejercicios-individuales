package edabit_nivel_intermedio;
import java.util.Arrays;
public class Programa_4 {
  public static int warOfNumbers(int[] numbers) {
		return Math.abs(
			Arrays.stream(numbers).reduce(0, (a, b) -> (b & 1) == 1 ? a + b : a - b)
		);
  }
}