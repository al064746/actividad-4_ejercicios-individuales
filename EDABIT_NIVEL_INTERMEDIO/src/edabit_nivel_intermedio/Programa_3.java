
package edabit_nivel_intermedio;

import java.util.Arrays;
public class Programa_3 {
	public static int sum(int[] arr) {
 if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0];
        }


        return arr[0] + sum(Arrays.copyOfRange(arr, 1, arr.length));
	}
}
